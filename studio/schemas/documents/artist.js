import { MdShoppingCart } from "react-icons/md";
import supportedLanguages from "../locale/supportedLanguages";

const baseLanguage = supportedLanguages.find((l) => l.isDefault);

export default {
  name: "artist",
  title: "Artist",
  description: "",
  type: "document",
  fields: [
    {
      name: "name",
      title: "Name",
      type: "localeString",
      validation: (rule) => rule.required(),
    },
    {
      name: "country",
      title: "Country",
      type: "localeString",
      // validation: (rule) => rule.required(),
    },
    {
      name: "shortDescription",
      title: "shortDescription",
      type: "localeBlockContent",
      // type: "localeString",
    },
    {
      name: "description",
      title: "Description",
      type: "localeBlockContent",
      // type: "localeString",
    },
    {
      name: "slug",
      title: "Slug",
      type: "localeSlug",
      validation: (rule) => rule.required(),
    },
    {
      name: "cover",
      title: "Cover",
      type: "image",
      options: {
        hotspot: true,
      },
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alt",
          options: {
            isHighlighted: true,
          },
        },
      ],
    },
    {
      name: "images",
      title: "Images",
      type: "array",
      of: [
        {
          name: "image",
          title: "Image",
          type: "image",
          options: {
            hotspot: true,
          },
          fields: [
            {
              name: "alt",
              type: "string",
              title: "Alt",
              options: {
                isHighlighted: true,
              },
            },
          ],
        },
      ],
    },
  ],

  preview: {
    select: {
      title: `name.${baseLanguage.id}`,
      subtitle: `slug.${baseLanguage.id}.current`,
      media: "images.0.images",
    },
    prepare({ title, subtitle, media }) {
      return {
        title: title,
        subtitle: `/${subtitle}`,
        media: media,
      };
    },
  },
};
