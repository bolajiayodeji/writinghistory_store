import { MdShoppingCart } from "react-icons/md";
import supportedLanguages from "../locale/supportedLanguages";

const baseLanguage = supportedLanguages.find((l) => l.isDefault);

export default {
  name: "product",
  title: "Products",
  description: "",
  type: "document",
  icon: MdShoppingCart,
  fields: [
    {
      name: "name",
      title: "Name",
      type: "localeString",
      validation: (rule) => rule.required(),
    },
    {
      name: "description",
      title: "Description",
      type: "localeBlockContent",
    },
    {
      name: "longDescription",
      title: "Long Description",
      type: "localeBlockContent",
    },
    {
      name: "slug",
      title: "Slug",
      type: "localeSlug",
      validation: (rule) => rule.required(),
    },
    {
      name: "reference",
      title: "Reference",
      type: "string",
      // validation: (rule) => rule.required(),
    },
    {
      name: "cover",
      title: "Cover",
      type: "image",
      options: {
        hotspot: true,
      },
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alt",
          options: {
            isHighlighted: true,
          },
        },
      ],
    },
    {
      name: "images",
      title: "Images",
      type: "array",
      of: [
        {
          name: "image",
          title: "Image",
          type: "image",
          options: {
            hotspot: true,
          },
          fields: [
            {
              name: "alt",
              type: "string",
              title: "Alt",
              options: {
                isHighlighted: true,
              },
            },
          ],
        },
      ],
    },
    {
      name: "variants",
      title: "Variants",
      type: "array",
      of: [
        {
          type: "reference",
          to: {
            type: "variant",
          },
        },
      ],
      // validation: (rule) => rule.required(),
    },
    {
      name: "artists",
      title: "Artists",
      type: "array",
      of: [
        {
          name: "artist",
          title: "Artist",
          type: "reference",
          to: [
            {
              type: "artist",
            },
          ],
        },
      ],
    },
  ],

  preview: {
    select: {
      title: `name.${baseLanguage.id}`,
      subtitle: `slug.${baseLanguage.id}.current`,
      media: "images.0.images",
    },
    prepare({ title, subtitle, media }) {
      return {
        title: title,
        subtitle: `/${subtitle}`,
        media: media,
      };
    },
  },
};
