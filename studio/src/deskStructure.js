import S from '@sanity/desk-tool/structure-builder';
import { createSuperPane } from 'sanity-super-pane';

export default () =>
  S.list()
    .items([
        // This returns an array of all the document types
      // defined in schema.js. We filter out those that we have
      // defined the structure above
      ...S.documentTypeListItems(),
      //S.listItem().title('Products').child(createSuperPane('product', S)),
      //S.listItem().title('Variant').child(createSuperPane('variant', S))
    ]);