import { useContext, useReducer, useEffect, useState } from "react";
import _ from "lodash";
import { useRouter } from "next/router";
import { useAppContext, useAppUpdateContext } from "../hooks/context";

export default function TaxonomiesFilter({ items, type, title }) {
  const router = useRouter();
  const lang = router.locale.toLowerCase().replace("-", "_");

  const [isAll, setIsAll] = useState(true);

  const context = useAppContext();
  const updateContext = useAppUpdateContext();

  useEffect(() => {
    let filter = context.categories;
    if (type == "artist") {
      filter = context.artists;
    }

    if (filter.length == 0) {
      setIsAll(true);
    } else {
      setIsAll(false);
    }
  }, [context]);

  const inputs = items.map((item) => {
    const itemName = item.name[`${lang}`];
    let filters = context.categories;
    if (type == "artist") {
      filters = context.artists;
    }

    return (
      <div className="control-container" key={item._id}>
        <label>{itemName}</label>
        <input
          type="checkbox"
          id={item._id}
          value={item._id}
          checked={filters.includes(item._id)}
          onChange={(e) => {
            updateContext.updateFilters(type, item._id);
          }}
        />
        <style jsx>
          {`
            .control-container {
              width: 100%;
              display: flex;
              justify-content: space-between;
              align-items: center;
            }
            label {
              font-size: 20px;
              font-weight: 700;
              line-height: 40px;
              text-transform: uppercase;
            }
          `}
        </style>
      </div>
    );
  });

  return (
    <>
      <div className="shop-category--column">
        <hr className="separator small" />
        <h3 className="taxonomy-title">{title}</h3>
        <div className="shop-category--controls"></div>
        <div>
          <div className="control-container">
            <label htmlFor="all">All</label>
            <input
              type="checkbox"
              value="*"
              id="all"
              checked={isAll}
              onChange={(ev) => {
                updateContext.updateFilters(type, null);
              }}
            />
          </div>

          {inputs}

          <style jsx>
            {`
              .control-container {
                width: 100%;
                display: flex;
                justify-content: space-between;
                align-items: center;
              }
              label {
                font-size: 20px;
                font-weight: 700;
                line-height: 40px;
                text-transform: uppercase;
              }
            `}
          </style>
        </div>
      </div>
    </>
  );
}
