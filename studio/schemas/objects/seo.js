export default {
    name: "seo",
    title: "Seo",
    type: "object",
    options: {
      collapsible: true,
      collpased: true,
    },
    fields: [
      {
        name: "title",
        title: "Title",
        type: "localeString",
        description:
          "The og will be auto generated from the title, if you want to overwrite, set this field.",
      },
      {
        name: "description",
        title: "Description",
        type: "localeText",
      },
      {
        name: "og_image",
        title: "Opengraph Image",
        type: "image",
        description:
          "The og will be auto generated from the cover field, if you want to overwrite, set this field.",
      },
    ],
  };
  