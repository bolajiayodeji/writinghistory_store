import React from "react";
import { useContext, useState } from "react";
import TaxonomiesFilter from "./TaxonomiesFilter";
import { useAppUpdateContext } from "../hooks/context";

export default function Sidebar({ categories, artists }) {
  const updateContext = useAppUpdateContext();

  return (
    <aside className="col-12 col-md-4 col-lg-3">
      <TaxonomiesFilter
        key="categories"
        title="Categories"
        items={categories}
        type={"category"}
      />

      <TaxonomiesFilter
        key="artists"
        title="Artists"
        items={artists}
        type={"artist"}
      />

      <button
        onClick={(ev) => {
          updateContext.updateFilters("artist", null);
          updateContext.updateFilters("category", null);
        }}
      >
        RESET FITLERS
      </button>
    </aside>
  );
}
