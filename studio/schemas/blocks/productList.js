export default {
  title: "Product list block",
  name: "productlistBlock",
  type: "object",
  fields: [
    {
      title: "Title",
      name: "title",
      type: "localeString",
    },
    {
      title: "Link",
      name: "link",
      type: "reference",
      to: [
        {
          type: "taxon",
        },
      ],
    },
    {
      name: "products",
      title: "Products",
      type: "array",
      of: [
        {
          name: "product",
          title: "Product",
          type: "reference",
          to: [
            {
              type: "product",
            },
          ],
        },
      ],
    },
    {
      name: "id",
      title: "Section ID",
      type: "string",
    },
  ],
  preview: {
    select: {
      //title: "title"
    },
    prepare(selection) {
      return Object.assign({ title: "Product list Block" });
    },
  },
};
