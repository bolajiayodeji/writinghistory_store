import Flickity from "react-flickity-component";
import { useRouter } from "next/router";
import Slide from "../cards/Slide";

export default function SliderArtists({ block }) {
  const router = useRouter();
  const lang = router.locale.toLowerCase().replace("-", "_");

  let artists = null;
  if (block.artists && block.artists.length > 0) {
    artists = block.artists.map((el) => {
      return <Slide key={el._id} id={el._id} artist={el} />;
    });
  }

  const flickityOptions = {
    cellAlign: "left",
    pageDots: false,
    contain: true,
    //wrapAround: true,
    arrowShape: {
      x0: 20,
      x1: 60,
      y1: 30,
      x2: 60,
      y2: 30,
      x3: 60,
    },
  };

  return (
    <section className="sliderArtist">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <hr className="separator small"></hr>
            <div className="row sliderArtist-header">
              <div className="col-6 d-flex align-items-center justify-content-start">
                <h3 className="sliderArtist-header--title">
                  {block.title.en_us}
                </h3>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <Flickity
              className={"slider row"} // default ''
              elementType={"div"} // default 'div'
              options={flickityOptions} // takes flickity options {}
              disableImagesLoaded={false} // default false
              reloadOnUpdate // default false
              static // default false
            >
              {artists}
            </Flickity>
          </div>
        </div>
      </div>

      <style jsx>{``}</style>

      <style global jsx>{`
        .sliderArtist {
          .sliderArtist-header {
            margin-bottom: 50px;

            .sliderArtist-header--title {
              font-weight: bold;
              font-size: 45px;

              .sliderArtist-content {
                padding: 35px 0 60px;
              }
              .sliderArtist-slide--title {
                font-size: 45px;
                font-weight: bold;

                .slider {
                  img {
                    width: 100%;
                    height: auto;
                  }
                }
              }
            }
          }
        }

        .flickity-prev-next-button.next {
          right: calc(var(--bs-gutter-x) * 0.5);
        }
        .flickity-prev-next-button.previous {
          right: 103px;
          left: auto;
        }
        .flickity-prev-next-button {
          top: -77px;
          width: 68px;
          height: 68px;
          background: transparent;
          border: 2px solid #000;
          border-radius: 0;
          transform: translateY(-50%);
        }
      `}</style>
    </section>
  );
}
