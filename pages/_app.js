import "../styles/main.scss";
import { AppWrapper } from "../hooks/context";

function MyApp({ Component, pageProps }) {
  return (
    <AppWrapper>
      <Component {...pageProps} />
    </AppWrapper>
  );
}

export default MyApp;
