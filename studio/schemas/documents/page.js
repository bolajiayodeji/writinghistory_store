import supportedLanguages from "../locale/supportedLanguages";
const baseLanguage = supportedLanguages.find((l) => l.isDefault);

export default {
  name: "page",
  title: "Pages",
  type: "document",
  fields: [
    {
      name: "name",
      title: "Name",
      type: "localeString",
      validation: (rule) => rule.required(),
    },
    {
      name: "slug",
      title: "Slug",
      type: "localeSlug",
      validation: (rule) => rule.required(),
    },
    {
      title: "Parent",
      name: "parent",
      type: "reference",
      to: [{ type: "page" }],
      options: {},
    },
    {
      name: "description",
      title: "Description",
      type: "localeBlockContent",
    },
    {
      title: "Blocks",
      name: "blocks",
      type: "array",
      of: [{ type: "megatitleBlock" }, { type: "productlistBlock" }, { type: "artistsliderBlock" }, { type: "artistslistBlock" }],
    },
    {
      title: "Cover",
      name: "cover",
      type: "image",
      options: {
        hotspot: true,
      },
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alt",
          options: {
            isHighlighted: true,
          },
        },
      ],
    },
    {
      name: "seo",
      title: "SEO",
      type: "seo",
    },
  ],

  preview: {
    select: {
      title: `name.${baseLanguage.id}`,
      subtitle: `slug.${baseLanguage.id}.current`,
      media: "images.0.images",
    },
    prepare({ title, subtitle, media }) {
      return {
        title: title,
        subtitle: `/${subtitle}`,
        media: media,
      };
    },
  },
};
