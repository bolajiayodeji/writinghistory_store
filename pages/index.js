import Layout from "../components/layout";
import MegatitleBlock from "../components/blocks/megatitleBlock";
import ProductsList from "../components/blocks/ProductsList";
import SliderArtists from "../components/blocks/SliderArtists";
import groq from "groq";
import { client } from "../utils/sanity/api";
import { useRouter } from "next/router";
import { useGetToken } from "../hooks/getToken";
import {
  CommerceLayer,
  OrderContainer,
  OrderStorage,
} from "@commercelayer/react-components";

export default function Index({
  data,
  preview,
  clientId,
  endpoint,
  marketId,
  countryCode,
}) {
  const router = useRouter();
  const lang = router.locale.toLowerCase().replace("-", "_");

  const post = data?.post;

  const token = useGetToken({
    clientId,
    endpoint,
    scope: marketId,
    countryCode: countryCode,
  });

  const blocks = post.blocks.map((el) => {
    if (el._type == "megatitleBlock") {
      return <MegatitleBlock block={el} key={el._key} />;
    }
    if (el._type == "productlistBlock") {
      return <ProductsList block={el} key={el._key} />;
    }
    if (el._type == "artistsliderBlock") {
      return <SliderArtists block={el} key={el._key} />;
    }
  });

  return (
    <>
      <CommerceLayer accessToken={token} endpoint={endpoint}>
        <OrderStorage persistKey={`order-${lang}`}>
          <OrderContainer attributes={{ lang }}>
            <Layout post={post}>{blocks}</Layout>
          </OrderContainer>
        </OrderStorage>
      </CommerceLayer>
    </>
  );
}

export async function getStaticProps({ locale, params, preview = false }) {
  const query = groq`*[_type=="home"][0]{
    ...,
    blocks[]{
      ...,
      _type == "productlistBlock" => {
        ...,
        products[]->{
          ...,
          artists[]->{
            name
          },
          variants[]->{...}
        }
      },
      _type== "artistsliderBlock" => {
        ...,
        artists[]->{
          ...
        },
        variants[]->{...}
      }
    }
  }`;
  //const countryCode = params?.countryCode || process.env.BUILD_COUNTRY?.toLowerCase()
  // Fetch necessary data for the blog post using params.slug
  const post = await client.fetch(query);
  return {
    props: {
      clientId: process.env.CL_CLIENT_ID,
      endpoint: process.env.CL_ENDPOINT,
      marketId: process.env.CL_MARKET,
      preview,
      countryCode: locale,
      data: {
        post: post,
      },
    },
  };
}
