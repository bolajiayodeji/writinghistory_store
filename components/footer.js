import Link from "next/link";
import { useRouter } from "next/router";

export default function Footer() {
  return (
    <>
      <footer id="footer">
        <div className="container-fluid">
          <div className="row footer-top">
            <div className="col-12">
              <hr className="separator big"></hr>
              <h2 className="footer-email">
                get in touch <br />{" "}
                <a href="mailto:mail@mail.com">mail@mail.com</a>
              </h2>
            </div>
          </div>
          <div className="row footer-bottom">
            <div className="col-12">
              <hr className="separator big"></hr>
              <div className="row">
                <div className="col-6 d-flex align-items-center justify-content-start">
                  <p>© 2021 Writing History - P.I 00000000000</p>
                </div>
                <div className="col-6 d-flex align-items-center justify-content-end">
                  <p>
                    <Link href="/privacy">
                      <a className="footer-link">Privacy</a>
                    </Link>{" "}
                    &#38;{" "}
                    <Link href="/cookie">
                      <a className="footer-link">Cookie</a>
                    </Link>{" "}
                    policies -{" "}
                    <Link href="/credits">
                      <a className="footer-link">Credits</a>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>

      <style jsx>{`
        #footer {
          p {
            font-weight: 700;
          }
        }

        .footer-top {
          margin-bottom: 35px;
        }

        .footer-email {
          margin: 0;
          //letter-spacing: 0.5px;
          //font-size: 120px;
          line-height: 1;
          font-size: 6.25vw;
          font-weight: 700;

          a {
            font-size: inherit;
          }
        }

        .footer-link {
          text-decoration: underline;

          &:active,
          &:focus,
          &:hover {
            color: rgba(black, 0.5);
            transition: 0.2s ease all;
          }
        }
      `}</style>

      <style global jsx>{``}</style>
    </>
  );
}
