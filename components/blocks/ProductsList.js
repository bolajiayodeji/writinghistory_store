import Link from "next/link";
import { useRouter } from "next/router";
import ProductCard from "../../components/cards/ProductCard";

export default function ProductsList({ block }) {
  const router = useRouter();
  const lang = router.locale.toLowerCase().replace("-", "_");

  let products = null;
  if (block.products && block.products.length > 0) {
    products = block.products.map((el) => {
      return <ProductCard key={el._id} id={el._id} product={el} />;
    });
  }

  return (
    <section className="productlist">
      <div className="container-fluid">
        <div className="row productlist-header">
          <div className="col-12">
            <hr className="separator big"></hr>
            <div className="row">
              <div className="col-6 d-flex align-items-center justify-content-start">
                <h3 className="productlist-title">{block.title.en_us}</h3>
              </div>
              <div className="col-6 d-flex align-items-center justify-content-end">
                <Link href="/shop">
                  <a className="productlist-link">view all {">>"}</a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="row productlist-content">{products}</div>

        <style jsx>{`
          .productlist-header {
            margin-bottom: 50px;
          }

          .productlist-content {
            margin-bottom: 90px;
          }

          .productlist-title {
            margin: 0;
            line-height: 1;
            font-size: 45px;
            //font-size: 2.35vw;
            font-weight: 700;
          }

          .productlist-link {
            font-size: 20px;
            font-weight: 700;
            text-decoration: underline;

            &:active,
            &:focus,
            &:hover {
              color: rgba(black, 0.5);
            }
          }
        `}</style>
      </div>
    </section>
  );
}
