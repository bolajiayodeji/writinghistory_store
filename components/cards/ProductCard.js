import { useRouter } from 'next/router'
import Link from 'next/link'
import Img from 'next/image'
import { useNextSanityImage } from 'next-sanity-image'
import { client } from '../../utils/sanity/api'
import _ from 'lodash'
import { Price, PricesContainer } from '@commercelayer/react-components'

export default function ProductCard({ product }) {
  const router = useRouter()
  const lang = router.locale.toLowerCase().replace('-', '_')
  const imageProps = useNextSanityImage(client, product?.cover)
  delete imageProps.width
  delete imageProps.height

  const code = _.first(product.variants)?.code

  let authorName = ''
  if (product.artists && product.artists.length > 0) {
    authorName = product.artists[0].name[`${lang}`]
  }

  return (
    <div className="card col-6 col-md-4">
      <Link href={`/product/${product.slug[lang].current}`}>
        <a className="card-link d-block">
          <div className="card-image">
            {product.cover !== null && (
              <Img
                {...imageProps}
                className="image"
                alt={product.cover.alt}
                layout="fill"
                objectFit="contain"
                quality="85"
                // sizes="(max-width: 360px)  150px, (max-width: 414px) 177px, (max-width: 768px) 226px, (max-width: 992px) 301px, (max-width: 1440px) 450px, 600px"
              />
            )}
          </div>
          <div className="card-content">
            <span className="card-author">{product.name[`${lang}`]}</span>
            <span className="card-title">{authorName}</span>
            <span className="card-price">
              <PricesContainer skuCode={code}>
                <Price className="" compareClassName="line-through" />
              </PricesContainer>
            </span>
          </div>
        </a>
      </Link>

      <style jsx>{`
        .card {
          margin-bottom: 50px;

          //  &:hover {
          //    .card-image img {
          //      transform: scale(1.05);
          //    }
          //  }
        }

        .card-image {
          width: 100%;
          height: 0;
          padding-bottom: 100%;
          position: relative;
          overflow: hidden;
          background: rgba(black, 0.03);
        }

        .card-content {
          //letter-spacing: 0.5px;
          margin-top: 30px;
          margin-bottom: 0;
        }

        .card-title,
        .card-author {
          //font-size: 45px;
          font-size: 2.35vw;
          display: block;
          line-height: 1.1;
        }

        .card-title {
          //letter-spacing: 0.5px;
          font-weight: 700;
        }

        .card-author {
          //letter-spacing: 0.5px;
          margin-top: 5px;
          font-weight: 400;
        }

        .card-price {
          margin-top: 15px;
          display: block;
          font-size: 20px;
          font-weight: 700;
        }
      `}</style>

      <style global jsx>{`
        .card-image {
          img {
            padding: 40px !important;
          }
        }
        .line-through {
          text-decoration: line-through;
        }
      `}</style>
    </div>
  )
}
