import React from "react";

export default function HeaderPage(props) {
  return (
    <section>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 d-flex align-items-center justify-content-start">
            <h1 className="page-title">{props.title} </h1>
          </div>
        </div>
      </div>

      <style jsx>{`
        .page-title {
          margin-top: 120px;
        }
      `}</style>
    </section>
  );
}
