import { useRouter } from 'next/router'
import Link from 'next/dist/client/link'
import ErrorPage from 'next/error'
import Layout from '../../components/layout'
import { client, getLocalePaths } from '../../utils/sanity/api'
import Img from 'next/image'
import { useNextSanityImage } from 'next-sanity-image'
import BlockContent from '@sanity/block-content-to-react'
import groq from 'groq'
import { useGetToken } from '../../hooks/getToken'
import {
  CommerceLayer,
  Price,
  PricesContainer,
  QuantitySelector,
  VariantsContainer,
  VariantSelector,
  OrderContainer,
  ItemContainer,
  AddToCartButton,
  OrderStorage,
} from '@commercelayer/react-components'
import _ from 'lodash'
import ProductCard from '../../components/cards/ProductCard'

export default function Product({
  data,
  preview,
  clientId,
  endpoint,
  marketId,
  countryCode,
}) {
  const router = useRouter()
  const token = useGetToken({
    clientId,
    endpoint,
    scope: marketId,
    countryCode: countryCode,
  })
  const product = data?.product
  const related = data?.related
  const artist = product.artists[0]
  const locale = router.locale
  const lang = locale.toLowerCase().replace('-', '_')
  const slug = data?.product?.slug[`${lang}`].current
  const imageProps = useNextSanityImage(client, data?.product.cover)
  const coverImageProps = useNextSanityImage(
    client,
    data?.product.artists[0].cover
  )
  const coverAlt = data?.product.artists[0].cover.alt
  delete imageProps.width
  delete imageProps.height

  const firstVariantCode = _.first(product?.variants)?.code
  /* const {
    data: { product },
  } = usePreviewSubscription(postQuery, {
    params: { slug },
    initialData: data,
    enabled: preview && slug,
  }) */

  if (!router.isFallback && !slug) {
    return <ErrorPage statusCode={404} />
  }
  if (!product?.name) {
    return <ErrorPage statusCode={404} />
  }

  let relatedProducts = null
  if (related && related.length > 0) {
    relatedProducts = related.map((el) => {
      return <ProductCard key={el._id} id={el._id} product={el} />
    })
  }

  const AddToCartCustom = (props) => {
    console.log(props);
    const { className, label, disabled, handleClick } = props
    const customHandleClick = async (e) => {
      const { success } = await handleClick(e)
      console.log(success);
    }
    return (
      <button
        disabled={disabled}
        className={className}
        onClick={customHandleClick}
      >
        {label}
      </button>
    )
  }

  const variantOptions = product?.variants?.map((variant) => {
    return {
      label: variant.size.name[lang],
      code: variant.code,
      lineItem: {
        name: product.name[lang],
        imageUrl: _.first(variant.images)?.asset.url,
      },
    }
  });

  console.log(variantOptions);
  

  return (
    <>
      <CommerceLayer accessToken={token} endpoint={endpoint}>
        <OrderStorage persistKey={`order-${countryCode}`}>
          <OrderContainer attributes={{ locale }}>
            <Layout post={product} preview={preview}>
              <ItemContainer>
                <section>
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-12">
                        <div className="row product-row">
                          <div className="col-6">
                            <div className="product-image">
                              {imageProps !== null && (
                                <Img
                                  {...imageProps}
                                  alt={product.cover.alt}
                                  layout="fill"
                                  objectFit="cover"
                                  quality="85"
                                  sizes="(max-width: 360px)  150px, (max-width: 414px) 177px, (max-width: 768px) 226px, (max-width: 992px) 301px, (max-width: 1440px) 450px, 600px"
                                />
                              )}
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="product-content">
                              <Link href={`/artists`}>
                                <a className="artist-link">{'<<'} back</a>
                              </Link>
                              <h2 className="author-name">
                                {artist.name[`${lang}`]}
                              </h2>

                              <h3 className="product-name">
                                {product.name[`${lang}`]}
                              </h3>
                              {<div className="controls-product">
                                <VariantsContainer>
                                  <VariantSelector
                                    options={_.sortBy(variantOptions, 'label')}
                                    className=""
                                  />
                                </VariantsContainer>
                              </div> }
                              <div className="sanity prices">
                       
                                <span>
                                  <PricesContainer>
                                    <Price
                                      skuCode={firstVariantCode}
                                      compareClassName="line-through"
                                    />
                                  </PricesContainer>
                                  <QuantitySelector />
                                  <AddToCartButton />
                                  {/* <AddToCartButton
                                    label={"Add to cart"}
                                    className="btn"
                                  >
                                    {AddToCartCustom}
                                  </AddToCartButton> */}
                                </span>

                                <BlockContent
                                  blocks={product.description[`${lang}`]}
                                  // imageOptions={{ w: 320, h: 240, fit: "max" }}
                                  {...client.config()}
                                />
                              </div>
                              <div className="controls-product">
                                <select
                                  name="product-quantity"
                                  id="product-quantity"
                                >
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                </select>

                                <button>Add to Cart</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-12 ">
                        <hr className="separator small start-description" />
                        <div className="longDescription-row">
                          <p>Description</p>
                          <div className="sanity">
                            <BlockContent
                              blocks={product.longDescription[`${lang}`]}
                              // imageOptions={{ w: 320, h: 240, fit: "max" }}
                              {...client.config()}
                            />
                          </div>
                        </div>
                        <hr className="separator small end-description" />
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-12">
                        <div className="row artist-row">
                          <div className="col-6">
                            <div className="artist-cover">
                              {coverImageProps !== null && (
                                <Img
                                  {...coverImageProps}
                                  alt={coverAlt}
                                  layout="responsive"
                                  quality="85"
                                  sizes="(max-width: 360px)  150px, (max-width: 414px) 177px, (max-width: 768px) 226px, (max-width: 992px) 301px, (max-width: 1440px) 450px, 600px"
                                />
                              )}
                            </div>
                          </div>
                          <div className="col-6 d-flex flex-column justify-content-center ">
                            <div className="artist-content--text">
                              <span>The Artist</span>
                              <span className="author-name description">
                                {artist.name[`${lang}`]}
                              </span>
                              <div className="sanity">
                                <BlockContent
                                  blocks={artist.shortDescription[`${lang}`]}
                                  // imageOptions={{ w: 320, h: 240, fit: "max" }}
                                  {...client.config()}
                                />
                              </div>
                              <Link
                                href={`/artist/${artist.slug[lang].current}`}
                              >
                                <a className="btn author-btn">Read More</a>
                              </Link>
                            </div>
                          </div>
                        </div>
                        <hr className="separator small" />
                      </div>
                    </div>

                    <div className="row related-products-row">
                      {relatedProducts}
                    </div>
                  </div>
                </section>
              </ItemContainer>
            </Layout>
          </OrderContainer>
        </OrderStorage>
      </CommerceLayer>

      <style jsx>{`
        .product-image {
          width: 100%;
          height: 0;
          padding-bottom: 100%;
          position: relative;
          overflow: hidden;
          background: rgba(black, 0.03);
        }

        .product-content,
        .artist-content--text {
          max-width: 450px;
          margin: 0 auto;
        }
        .artist-link {
          text-decoration: underline;
          margin-bottom: 40px;
          display: block;
          &:active,
          &:focus,
          &:hover {
            color: rgba(black, 0.5);
            transition: 0.2s ease all;
          }
        }

        .line-through {
          margin-bottom: 50px;
        }

        .product-name,
        .author-name {
          font-size: 45px;
        }
        .author-name {
          font-weight: 700;
          &.description {
            margin-top: 25px;
            margin-bottom: 45px;
          }
        }

        .product-name {
          line-height: 1;
          font-weight: 400;
          margin-bottom: 45px;
        }
        .controls-product {
          margin-top: 50px;
          max-height: 120px;
          display: flex;
          font-size: 20px;

          select {
            border-radius: 0;
            padding: 15px;
            width: 20%;
            text-align: center;
            border: 2px solid #000;

            &:focus-visible {
              border-radius: 0px !important;
              border-color: #000;
            }
          }
        }

        .btn,
        button {
          display: block;
          font-weight: 700;
          text-align: center;
          text-transform: uppercase;
          width: 100%;
          padding: 15px;
          border: none;
          border-radius: 0%;
          background: #000;
          color: #fff;
          &:hover {
            color: #000;
            background: rgba(black, 0.03);
            transition: 0.3s ease-in-out all;
          }
        }

        .author-btn {
          margin-top: 65px;
        }
        .longDescription-row {
          margin: 50px 0;
        }

        .artist-row {
          margin-top: 70px;
          margin-bottom: 110px;
        }

        .product-row {
          margin-bottom: 110px;
        }

        .artist-text--content {
          max-width: 450px;
        }
      `}</style>

      <style global jsx>{`
        .product-image {
          img {
            padding: 60px !important;
          }
        }

        .longDescription {
          font-size: 45px;
          font-weight: 700;
        }
      `}</style>
    </>
  )
}

export async function getStaticPaths({ locales, defaultLocale }) {
  return getLocalePaths('product', locales, defaultLocale, true)
}

export async function getStaticProps({ locale, params, preview = false }) {
  const lang = locale.toLowerCase().replace('-', '_')
  let query = groq`*[_type=='product' && slug.en_us.current == $slug][0]{ 
    ..., 
    artists[]->{
      ...
    }, 
    variants[]->{
      ...,
      images[]{asset->{...}},
      size->{...}
    }
  }`
  if (lang == 'it_it') {
    query = groq`*[_type=='product' && slug.it_it.current == $slug][0]{ ..., artists[]->{
      ...
    }, variants[]->{..., images[]{...}, size->{...}},
     artists[]->{
            name
          }
  }`
  }
  //const countryCode = params?.countryCode || process.env.BUILD_COUNTRY?.toLowerCase()
  const { slug = '' } = params
  const product = await client.fetch(query, { slug: slug, lang: lang })

  let artistsId = []
  product.artists.forEach((el) => {
    artistsId.push(el._id)
  })
  console.log(artistsId)
  let queryRelated = groq`*[_type=='product' && references($artistsId)]{ 
    ..., 
    artists[]->{
      name
    }, 
    variants[]->{
      ...,
      images[]->{...},
      size->{...}
    }
  }`
  const related = await client.fetch(queryRelated, { artistsId: artistsId })
  //console.log("product", product);
  return {
    props: {
      clientId: process.env.CL_CLIENT_ID,
      endpoint: process.env.CL_ENDPOINT,
      marketId: process.env.CL_MARKET,
      preview,
      countryCode: locale,
      data: {
        related: related,
        product: product,
      },
    },
  }
}
