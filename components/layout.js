import Meta from "../components/meta";
import Header from "../components/header";
import Footer from "../components/footer";

export default function Layout({ post, children }) {
  return (
    <>
      <Meta post={post} />
      <Header />
      <main>{children}</main>
      <Footer />
    </>
  );
}
