import groq from "groq";

const product = groq`{
   ..., 
    artists[]->{
        name
    },
    variants[]->{...}
}`;

export function getProducts(artists, categories, page, perPage) {

    const truncate = page * perPage;
    let selector = `_type == "product"`;

    //variations
    if (artists.length > 0 && categories.length == 0) {
        selector = groq`_type=="product" && references($artists)`;
    }

    if (categories.length > 0 && artists.length == 0) {
        selector = groq`_type == "product" && _id in *[_type == "taxon" && _id in $categories].products[]._ref`
    }

    if (categories.length > 0 && artists.length > 0) {
        selector = groq`_type == "product" && references($artists) && _id in *[_type == "taxon" && _id in $categories].products[]._ref`;
    }

    return groq`{ 
        "products": *[${selector}]|order(_createdAt asc)[0...${truncate}]${product},
        "total": count(*[${selector}])
    }`;
}