export default {
  name: "home",
  title: "Homepage",
  type: "document",
  fields: [
    {
      title: "Blocks",
      name: "blocks",
      type: "array",
      of: [{ type: "megatitleBlock" }, { type: "productlistBlock" }, { type: "artistsliderBlock" }],
    },
    {
      name: "seo",
      title: "SEO",
      type: "seo",
    },
  ],

  preview: {
    select: {},
    prepare({ lang }) {
      return {
        title: "Home",
      };
    },
  },
};
