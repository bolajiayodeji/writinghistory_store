export default {
  title: "Mega title block",
  name: "megatitleBlock",
  type: "object",
  fields: [
    {
      title: "Content",
      name: "content",
      type: "localeBlockContent",
    },
    {
      name: "images",
      title: "Images",
      type: "array",
      of: [
        {
          name: "image",
          title: "Image",
          type: "image",
          options: {
            hotspot: true,
          },
          fields: [
            {
              name: "alt",
              type: "string",
              title: "Alt",
              options: {
                isHighlighted: true,
              },
            },
          ],
        },
      ],
    },
    {
      name: "id",
      title: "Section ID",
      type: "string",
    },
  ],
  preview: {
    select: {
      //title: "title"
    },
    prepare(selection) {
      return Object.assign({ title: "Mega Title Block" });
    },
  },
};
