import Layout from '../../components/layout'
import groq from 'groq'
import { client } from '../../utils/sanity/api'
import Sidebar from '../../components/Sidebar'
import HeaderPage from '../../components/HeaderPage'
import ProductsList from '../../components/ProductsList'
import { useAppContext, useAppUpdateContext } from '../../hooks/context'
import { useRouter } from 'next/router'
import {
  CommerceLayer,
  OrderContainer,
  OrderStorage,
} from '@commercelayer/react-components'
import { useGetToken } from '../../hooks/getToken'

export default function Index({
  data,
  preview,
  clientId,
  endpoint,
  marketId,
  countryCode,
}) {
  const categories = data?.categories
  const artists = data?.artists
  const router = useRouter()
  const lang = router.locale.toLowerCase().replace('-', '_')

  const context = useAppContext()
  const updateContext = useAppUpdateContext()

  const token = useGetToken({
    clientId,
    endpoint,
    scope: marketId,
    countryCode: countryCode,
  })

  return (
    <>
      <CommerceLayer accessToken={token} endpoint={endpoint}>
        <OrderStorage persistKey={`order-${lang}`}>
          <OrderContainer attributes={{ lang }}>
            <Layout>
              <HeaderPage title="Shop" />
              <section className="shop-section">
                <div className="container-fluid">
                  <div className="row">
                    {/* <div className="col-12 col-md-6"> */}
                    <div className="col-12">
                      <div className="row">
                        <Sidebar categories={categories} artists={artists} />
                        <div className="col-12 col-md-8 col-lg-9">
                          <hr className="separator small" />
                          <div className="shop-products--controls d-flex justify-content-between">
                            <span className="numbers-of-products">
                              {context.page * context.perPage} of{' '}
                              {context.total}
                            </span>
                            <div className="orderBy">
                              <label>order by: </label>
                              <select
                                value={context.orderBy}
                                onChange={(ev) => {
                                  updateContext.updateOrderBy(ev.target.value)
                                }}
                                className="selection"
                              >
                                <option value="default">Default</option>
                                <option value="name-desc">Name A..Z</option>
                                <option value="name-asc">Name Z..A</option>
                              </select>
                            </div>
                          </div>
                          <ProductsList />

                          <div className="col-12">
                            {context.page * context.perPage != context.total ? (
                              <button
                                onClick={(ev) => {
                                  updateContext.updatePage()
                                }}
                              >
                                Show More
                              </button>
                            ) : (
                              ''
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </Layout>
          </OrderContainer>
        </OrderStorage>
      </CommerceLayer>
      <style jsx>{`
        section {
          margin-top: 100px;
          margin-bottom: 160px;
        }

        .shop-products--controls {
          margin-bottom: 35px;
        }
        label,
        select,
        .numbers-of-products {
          font-size: 20px;
          letter-spacing: 0.5;
          font-weight: 700;
        }
        select {
          border: none;
          text-decoration: underline;
          cursor: pointer;
        }

        .products-container {
          margin-top: 30px;
        }
      `}</style>
    </>
  )
}

export async function getStaticProps({ locale, params, preview = false }) {
  const query = groq`*[_type=="home"][0]{
    ...
  }`

  const categories =
    await client.fetch(groq`*[_type=="taxon"]|order(name.en_us asc){
    ...
  }`)

  const artists =
    await client.fetch(groq`*[_type=="artist"]|order(name.en_us asc){
    ...
  }`)

  //const countryCode = params?.countryCode || process.env.BUILD_COUNTRY?.toLowerCase()
  // Fetch necessary data for the blog post using params.slug
  const post = await client.fetch(query)
  return {
    props: {
      clientId: process.env.CL_CLIENT_ID,
      endpoint: process.env.CL_ENDPOINT,
      marketId: process.env.CL_MARKET,
      preview,
      countryCode: locale,
      data: {
        categories: categories,
        artists: artists,
        post: post,
      },
    },
  }
}

// /shop/aegp / 3;
