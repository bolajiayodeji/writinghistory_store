import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import { getSalesChannelToken } from "@commercelayer/js-auth";

export function useGetToken({
  clientId,
  endpoint,
  countryCode,
  scope = "market:all",
}) {
  console.log(clientId, endpoint, countryCode, scope);
  const [token, setToken] = useState("");
  useEffect(() => {
    const getCookieToken = Cookies.get(`clAccessToken`);
    if (!getCookieToken && clientId && endpoint) {
      const getToken = async () => {
        const auth = await getSalesChannelToken({
          clientId,
          endpoint,
          scope, // NOTE: take it from country
        });
        setToken(auth?.accessToken); // TODO: add to LocalStorage
        Cookies.set(`clAccessToken`, auth?.accessToken, {
          expires: auth?.expires,
        });
      };
      getToken();
    } else {
      setToken(getCookieToken || "");
    }
  });
  return token;
}
