import Link from "next/link";

export default function Header() {
  return (
    <>
      <header id="header">
        <div className="container-fluid">
          <div className="row menu">
            <div className="col-8">
              <Link href="/">
                <a>logo</a>
              </Link>
            </div>
            <div className="col-4">
              <nav>
                <ul>
                  <li>
                    <Link href="/artists">
                      <a className="header-link">Artists</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/shop">
                      <a className="header-link">Shop</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/about">
                      <a className="header-link">About</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/cart">
                      <a className="header-link">Cart</a>
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>

      <style jsx>{`
        header,
        .menu {
          height: 10vh;
          align-items: center;
        }

        nav {
          display: flex;
          justify-content: flex-end;

          ul {
            font-weight: 700;
            display: flex;

            li {
              margin-bottom: 0;
              margin-left: 50px;
              list-style: none;
            }
          }
        }
        .header-link {
          &:active,
          &:focus,
          &:hover {
            color: rgba(black, 0.5);
            transition: 0.2s ease all;
          }
        }
      `}</style>
    </>
  );
}
