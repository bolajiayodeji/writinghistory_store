import BlockContent from "@sanity/block-content-to-react";
import { client } from "../../utils/sanity/api";
import { useRouter } from "next/router";

export default function megatitleBlock({ block }) {
  const router = useRouter();
  const lang = router.locale.toLowerCase().replace("-", "_");

  return (
    <div className="firstsection">
      <section className="container-fluid">
        <div className="row">
          <div className="col-12">
            <hr className="separator big"></hr>
            <BlockContent
              blocks={block.content[`${lang}`]}
              // imageOptions={{ w: 320, h: 240, fit: "max" }}
              {...client.config()}
            />
          </div>
        </div>
      </section>
      <style global jsx>{`
        h2 {
          //font-size: 120px;
          font-size: 6.25vw;
          font-weight: bold;
          line-height: 1;
          margin-bottom: 0px;
        }

        span {
          &:hover {
            transition: 0.5 all ease;
            text-decoration: underline;
            cursor: pointer;
          }
        }
      `}</style>
    </div>
  );
}
