import React, { useState } from 'react'
import Link from 'next/link'
import _ from 'lodash'
import { useRouter } from 'next/router'
import SingleArtist from '../cards/SingleArtist'

export default function ArtistsListBlock({ block }) {
  const router = useRouter()
  const lang = router.locale.toLowerCase().replace('-', '_')

  const [orderBy, setOrderBy] = useState('asc')
  const [sorted, setSorted] = useState(block.artists)

  const sortArrayHandler = (event) => {
    setOrderBy(event.target.value)
    const orderd = _.orderBy(sorted, [`name[${lang}]`], [event.target.value])
    setSorted(orderd)

    // console.log(sorted);
    // console.log(event);
  }

  let artists = null
  if (block.artists && block.artists.length > 0) {
    artists = sorted.map((el) => {
      return <SingleArtist key={el._id} id={el._id} artist={el} />
    })
  }

  return (
    <section className="artistList-section">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 d-flex align-items-center justify-content-end">
            <div className="orderBy">
              <label>order by: </label>
              <select
                value={orderBy}
                onChange={sortArrayHandler}
                className="selection"
                name="artists"
                id="artists"
              >
                <option value="asc">A to Z</option>
                <option value="desc">Z to A</option>
              </select>
            </div>
          </div>
        </div>

        <div className="row artistlist-content">
          <div className="col-12">
            <ul>{artists}</ul>
          </div>
        </div>
      </div>
      <style jsx>{`
        select {
          //font-size: 45px;
          font-size: 2.35vw;
          font-weight: 700;
          border: none;
          text-decoration: underline;
          cursor: pointer;
          background-color: transparent;
        }
        label {
          letter-spacing: 0.5;
          font-size: 2.35vw;
          font-weight: 700;
        }
        .orderBy {
          margin: 160px 0 80px;
        }
      `}</style>
    </section>
  )
}
