import React from "react";
import { useRouter } from "next/router";
import Link from "next/dist/client/link";
import Img from "next/image";
import { useNextSanityImage } from "next-sanity-image";
import { client } from "../../utils/sanity/api";

export default function SingleArtist({ artist }) {
  const router = useRouter();
  const lang = router.locale.toLowerCase().replace("-", "_");

  const imageProps = useNextSanityImage(client, artist?.cover);

  // console.log(imageProps);
  // console.log(typeof artist.cover);

  return (
    <>
      <li className="artist-list">
        <Link href={`/artist/${artist.slug[lang].current}`}>
          <a>
            <div className="row">
              <div className="col-12 artist-list--content">
                <h3 className="artist-list--name">{artist.name[`${lang}`]}</h3>
                <div className="image-container">
                  {imageProps !== null && (
                    <Img
                      {...imageProps}
                      layout="responsive"
                      quality="85"
                      sizes="(max-width: 360px)  150px, (max-width: 414px) 177px, (max-width: 768px) 226px, (max-width: 992px) 301px, (max-width: 1440px) 450px, 600px"
                    />
                  )}
                </div>
              </div>
            </div>
          </a>
        </Link>
      </li>

      <style jsx>{`
        .artist-list {
          position: relative;
          border-top: 13px solid #000;

          &:hover {
            transition: 0.5s ease all;
            background-color: #000;
            color: #fff;
            .artist-list--name {
              color: #fff;
            }
            .image-container {
              opacity: 1;
            }
          }

          .artist-list--name {
            //font-size: 45px;
            font-size: 2.35vw;
            font-weight: 700;
            padding: 30px 20px;
          }
        }
        .image-container {
          position: absolute;
          left: 50%;
          top: 50%;
          transform: translate(-50%, -50%);
          width: 30%;
          opacity: 0;
          z-index: 2;
          transition: 0.5s ease-in-out all;
          pointer-events: none;
        }

        .separator.list {
          margin-bottom: 0px;
          margin-left: 5px;
        }
      `}</style>
    </>
  );
}
