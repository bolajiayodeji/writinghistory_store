import { createContext, useContext, useEffect, useState } from 'react'
import { client } from '../utils/sanity/api'
import groq from 'groq'
import { getProducts } from '../utils/sanity/queries'
import _ from 'lodash'
import { useRouter } from 'next/router'

const AppContext = createContext()
const AppUpdateContext = createContext()

export const loadProducts = async (artists, categories, page, perPage) => {
  console.log('load', artists, categories, page, perPage)

  let query = getProducts(artists, categories, page, perPage)

  console.log(query)

  const results = await client.fetch(query, {
    artists: artists,
    categories: categories,
    truncate: page * perPage,
  })

  return results
}

export const orderProducts = (orderBy, lang, products) => {
  console.log('reorder', orderBy, lang, products)
  let conditions = orderBy.split('-')
  let field = ''
  let order = 'asc'
  if (conditions[0] == 'name') {
    field = `name.${lang}`
    order = conditions[1]
  }
  if (conditions[0] == 'default') {
    field = `_createdAt`
  }
  const orderedProducts = _.orderBy(products, [field], [order])
  return orderedProducts
}

export function AppWrapper({ children }) {
  const router = useRouter()
  const lang = router.locale.toLowerCase().replace('-', '_')

  const [products, setProducts] = useState([])
  const [artists, setArtists] = useState([])
  const [categories, setCategories] = useState([])
  const [page, setPage] = useState(1)
  const [perPage, setPerPage] = useState(3)
  const [total, setTotal] = useState(0)
  const [orderBy, setOrderBy] = useState('default')

  useEffect(() => {
    //fetch products
    loadProducts(artists, categories, page, perPage).then((result) => {
      //reorder by current setup
      setProducts(orderProducts(orderBy, lang, result.products))
      setTotal(result.total)
    })
  }, [artists, categories, page, perPage])

  useEffect(() => {
    if (products.length > 0) {
      //order products
      setProducts(orderProducts(orderBy, lang, products))
    }
  }, [orderBy])

  const sharedState = {
    products,
    categories,
    artists,
    page,
    perPage,
    total,
    orderBy,
  }

  const updateFns = {
    updateFilters: (type, id) => {
      let filters = categories
      if (type == 'artist') {
        filters = artists
      }

      //check if is a reset
      if (id == null) {
        if (type == 'artist') {
          setArtists([])
        } else {
          setCategories([])
        }
        setPage(1)
        return
      }

      //check if id is already present
      if (filters.includes(id)) {
        //remove form array
        let filtersUpdated = _.remove(filters, (e) => {
          return e != id
        })
        if (type == 'artist') {
          setArtists(filtersUpdated)
        } else {
          setCategories(filtersUpdated)
        }
      } else {
        //add to array
        if (type == 'artist') {
          setArtists([...filters, id])
        } else {
          setCategories([...filters, id])
        }
      }
    },
    updatePage: () => {
      setPage(page + 1)
    },
    updateOrderBy: (order) => {
      setOrderBy(order)
    },
  }

  return (
    <AppContext.Provider value={sharedState}>
      <AppUpdateContext.Provider value={updateFns}>
        {children}
      </AppUpdateContext.Provider>
    </AppContext.Provider>
  )
}

export function useAppContext() {
  return useContext(AppContext)
}

export function useAppUpdateContext() {
  return useContext(AppUpdateContext)
}
