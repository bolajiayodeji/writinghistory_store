// client.js
import sanityClient from '@sanity/client'
import groq from "groq";
import _ from "lodash";

export const client = sanityClient({
  projectId: '7npoti9c', // you can find this in sanity.json
  dataset: 'production', // or the name you chose in step 1
  useCdn: false, // `false` if you want to ensure fresh data,
  apiVersion: "2021-11-19"
})


export async function getLocalePaths(type, locales, defaultLocale, fallback) {
  let localePaths = await Promise.all(
    locales.map(async (locale) => {
      const lang = locale.toLowerCase().replace("-", "_");
      return await client.fetch(
        groq`*[_type == $type && defined(slug) && defined(slug[$lang])][]{ "params": {"slug": slug[$lang].current}, "locale": $locale}`, {lang: lang, locale: locale, type: type}
      );
    })
  );

  //console.log(localePaths);
  localePaths = _.flatten(localePaths);
  localePaths = _.map(localePaths, (o) => {
    if (o.locale == defaultLocale) {
      delete o.locale;
      return o;
    } else {
      return o;
    }
  })
  //console.log(localePaths);

  return {
    paths: localePaths,
    fallback: true,
  };

}