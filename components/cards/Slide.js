import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Img from "next/image";
import { useNextSanityImage } from "next-sanity-image";
import { client } from "../../utils/sanity/api";

export default function Slide({ artist }) {
  const router = useRouter();
  const lang = router.locale.toLowerCase().replace("-", "_");

  const imageProps = useNextSanityImage(client, artist?.cover);

  return (
    <div className="slide col-6">
      {/* <Link href={"/artist/" + artist.slug[`${lang}`].current}> */}
      <Link href={`/artist/${artist.slug[lang].current}`}>
        <a>
          {/* {artist.cover && ( */}
          {artist.cover !== null && (
            <Img
              {...imageProps}
              layout="responsive"
              quality="85"
              sizes="(max-width: 360px)  150px, (max-width: 414px) 177px, (max-width: 768px) 226px, (max-width: 992px) 301px, (max-width: 1440px) 450px, 600px"
            />
          )}

          <h3 className="sliderArtist-slide--title">
            {artist.name[`${lang}`]}
          </h3>
        </a>
      </Link>

      <style jsx>{``}</style>

      <style global jsx>{`
        .sliderArtist-slide--title {
          padding: 35px 0 60px;
          font-size: 45px;
          font-weight: bold;
        }
        .slide {
          padding-right: calc(var(--bs-gutter-x) * 0.5);
          padding-left: calc(var(--bs-gutter-x) * 0.5);
        }
      `}</style>
    </div>
  );
}
