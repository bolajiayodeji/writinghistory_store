import React from "react";

const spanRender = (props) => (
  <span style={{ backgroundColor: "lightgrey" }}>{props.children}</span>
);
const smallRender = (props) => <small>{props.children}</small>;
const underlineRender = (props) => <u>{props.children}</u>;
const supRender = (props) => <sup>{props.children}</sup>
/**
 * This is the schema definition for the rich text fields used for
 * for this blog studio. When you import it in schemas.js it can be
 * reused in other parts of the studio with:
 *  {
 *    name: 'someName',
 *    title: 'Some title',
 *    type: 'blockContent'
 *  }
 */
export default {
  title: "Block Content",
  name: "blockContent",
  type: "array",
  of: [
    {
      title: "Block",
      type: "block",
      // Styles let you set what your user can mark up blocks with. These
      // corrensponds with HTML tags, but you can set any title or value
      // you want and decide how you want to deal with it where you want to
      // use your content.
      styles: [
        { title: "Normal", value: "normal" },
        { title: "H1", value: "h1" },
        { title: "H2", value: "h2" },
        { title: "H3", value: "h3" },
        { title: "H4", value: "h4" },
        { title: "H5", value: "h5" },
        { title: "H6", value: "h6" },
        { title: "Quote", value: "blockquote" },
      ],
      lists: [{ title: "Bullet", value: "bullet" }],
      // Marks let you mark up inline text in the block editor.
      marks: {
        // Decorators usually describe a single property – e.g. a typographic
        // preference or highlighting by editors.
        decorators: [
          { title: "Span", value: "span" },
          { title: "Strong", value: "strong" },
          { title: "Emphasis", value: "em" },
          { title: "Apix", value: "sup", blockEditor: {
            render: supRender,
          }, },
        ],
        // Annotations can be any object structure – e.g. a link or a footnote.
        annotations: [
          {
            title: "URL",
            name: "link",
            type: "object",
            fields: [
              {
                title: "URL",
                name: "href",
                type: "url",
                validation: (Rule) =>
                  Rule.uri({
                    allowRelative: true,
                    scheme: ["https", "http", "mailto", "tel"],
                  }),
              },
              /* {
                title: "Internal Link",
                name: "internal",
                type: "reference",
                to: [
                  { type: "page" },
                  { type: "typology" },
                  { type: "product" },
                  { type: "application" },
                  { type: "academy" },
                ],
              }, */
              {
                title: "Attachment",
                name: "attachment",
                type: "file",
              },
              {
                title: "ScrollTo",
                name: "scrollTo",
                type: "boolean",
              },
            ],
          },
        ],
      },
    },
    // You can add additional types here. Note that you can't use
    // primitive types such as 'string' and 'number' in the same array
    // as a block type.
    {
      type: "image",
      options: { hotspot: true },
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alt",
          options: {
            isHighlighted: true,
          },
        },
      ],
    },
    /* {
      type: "inlineTable",
    }, */
    /* {
      type: "inlineCTA"
    } */
  ],
};
