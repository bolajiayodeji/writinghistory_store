export default {
  title: "Artists slider block",
  name: "artistsliderBlock",
  type: "object",
  fields: [
    {
      title: "Title",
      name: "title",
      type: "localeString",
    },
    {
      name: "artists",
      title: "Artists",
      type: "array",
      of: [
        {
          name: "artist",
          title: "Artist",
          type: "reference",
          to: [
            {
              type: "artist",
            },
          ],
        },
      ],
    },
    {
      name: "id",
      title: "Section ID",
      type: "string",
    },
  ],
  preview: {
    select: {
      //title: "title"
    },
    prepare(selection) {
      return Object.assign({ title: "Artist slider Block" });
    },
  },
};
