import { useRouter } from 'next/router'
import { useState, useEffect, useContext } from 'react'
import ProductCard from '../components/cards/ProductCard'
import { useAppContext } from '../hooks/context'

export default function ProductsList({}) {
  const router = useRouter()
  const lang = router.locale.toLowerCase().replace('-', '_')

  const context = useAppContext()
  const products = context?.products

  let productsItems = null
  if (products && products.length > 0) {
    productsItems = products.map((el) => {
      return <ProductCard key={el._id} id={el._id} product={el} />
    })
  }

  return (
    <section className="productlist">
      <div className="row productlist-content">{productsItems}</div>
      <style jsx>{`
        .productlist-header {
          margin-bottom: 50px;
        }

        .productlist-content {
          margin-bottom: 90px;
        }

        .productlist-title {
          margin: 0;
          line-height: 1;
          font-size: 45px;
          //font-size: 2.35vw;
          font-weight: 700;
        }

        .productlist-link {
          font-size: 20px;
          font-weight: 700;
          text-decoration: underline;

          &:active,
          &:focus,
          &:hover {
            color: rgba(black, 0.5);
          }
        }
      `}</style>
    </section>
  )
}
