export default {
  title: "Artists List block",
  name: "artistslistBlock",
  type: "object",
  fields: [
    {
      title: "Enabled",
      name: "enbled",
      type: "boolean",
    },
  ],
  preview: {
    select: {
      //title: "title"
    },
    prepare(selection) {
      return Object.assign({ title: "Artist list Block" });
    },
  },
};
