import React, { Children } from "react";
import ErrorPage from "next/error";
import BlockContent from "@sanity/block-content-to-react";
import { useRouter } from "next/router";
import Link from "next/dist/client/link";
import Img from "next/image";
import groq from "groq";
import { useNextSanityImage } from "next-sanity-image";
import { client, getLocalePaths } from "../../utils/sanity/api";
import Layout from "../../components/layout";
import ProductCard from "../../components/cards/ProductCard";

export default function Artist({ data }) {
  const router = useRouter();
  const artist = data?.artist;
  const locale = router.locale;
  const lang = locale.toLowerCase().replace("-", "_");
  const slug = data?.artist?.slug[`${lang}`].current;

  const imageProps = useNextSanityImage(client, artist?.cover);
  delete imageProps.width;
  delete imageProps.height;

  if (!router.isFallback && !slug) {
    return <ErrorPage statusCode={404} />;
  }

  if (!artist?.name) {
    return <ErrorPage statusCode={404} />;
  }

  let artistProducts = null;
  if (data.artist.products && data.artist.products.length > 0) {
    artistProducts = data.artist.products.map((el) => {
      return <ProductCard key={el._id} id={el._id} product={el} />;
    });
  }

  return (
    <Layout post={artist}>
      <section className="artist">
        <div className="container-fluid">
          <hr className="separator big"></hr>
          <div className="row artist-row">
            <div className="col-12 col-md-6">
              <div className="artist-image">
                {imageProps !== null && (
                  <Img
                    {...imageProps}
                    layout="fill"
                    objectFit="cover"
                    quality="85"
                    sizes="(max-width: 360px)  150px, (max-width: 414px) 177px, (max-width: 768px) 226px, (max-width: 992px) 301px, (max-width: 1440px) 450px, 600px"
                  />
                )}
              </div>
            </div>
            <div className="col-12 col-md-6">
              <div className="artist-content">
                <Link href={`/artists`}>
                  <a className="artist-link">{"<<"} back</a>
                </Link>
                <span className="artist-name">{artist.name[`${lang}`]}</span>
                <span className="artist-country">
                  {artist.country[`${lang}`]}
                </span>
                <span className="artist-description">
                  <BlockContent
                    blocks={artist.description[`${lang}`]}
                    // imageOptions={{ w: 320, h: 240, fit: "max" }}
                    {...client.config()}
                  />
                </span>
              </div>
            </div>
          </div>

          <div className="row productlist-header">
            <div className="col-12">
              <hr className="separator big"></hr>
            </div>
          </div>

          <div className="row">
            <div className="col-6 d-flex align-items-center justify-content-start">
              <h3 className="productlist-title">Works</h3>
            </div>
            <div className="col-6 d-flex align-items-center justify-content-end">
              <Link href="/shop">
                <a className="artist-link">view all {">>"}</a>
              </Link>
            </div>
          </div>

          <div className="row">{artistProducts}</div>

          <div className="row">
            <div className="col-12 nextArtist-row">
              <hr className="separator small" />

              <div className="row nextArtist-scroll-container align-items-center justify-content-center ">
                <div className="col-6">
                  <Link href={`/artist/${slug}`}>
                    <a className="artist-name">
                      Next Artist: {artist.name[`${lang}`]}
                    </a>
                  </Link>
                </div>

                <div className="col-6">
                  {/* <Link href={`/artist/${artist.slug[lang].current}`}> */}
                  <Link href={`/artist/${slug}`}>
                    <a className="artist-name">
                      Next Artist: {artist.name[`${lang}`]}
                    </a>
                  </Link>
                </div>
              </div>
              <hr className="separator small" />
            </div>
          </div>
        </div>

        <style jsx>{`
          .artist {
            margin-bottom: 110px;
          }

          .separator.big {
            margin-bottom: 0;
          }

          .artist-row {
            margin-bottom: 110px;
          }

          .artist-image {
            // width: 100%;
            // height: 0;
            padding-bottom: 100%;
            position: relative;
            overflow: hidden;
            background: rgba(black, 0.03);
          }

          .productlist-header {
            margin-bottom: 50px;
          }
          .productlist-title {
            font-size: 45px;
            font-weight: 700;
          }

          .artist-content {
            max-width: 450px;
            margin: 0 auto;
          }

          .artist-link {
            display: block;
            margin-top: 30px;
            margin-bottom: 45px;
            font-size: 20px;
            font-weight: 700;
            text-decoration: underline;

            &:active,
            &:focus,
            &:hover {
              color: rgba(black, 0.5);
              transition: 0.2s ease all;
            }
          }

          .artist-name {
            margin-bottom: 30px;
            font-size: 45px;
          }

          .artist-country,
          .artist-description {
            letter-spacing: 0.3;
            line-height: 1.4;
            font-weight: 700;
          }

          .artist-country {
            margin: 55px 0;
            letter-spacing: 0.5;
            text-transform: uppercase;
          }

          .nextArtist-scroll-container {
            margin-bottom: 30px;
            animation: scrollingText 15s linear infinite;
          }

          @keyframes scrollingText {
            0% {
              transform: translateX(0);
            }
            100% {
              transform: translateX(-600px);
            }
          }
        `}</style>
      </section>
    </Layout>
  );
}

export async function getStaticPaths({ locales, defaultLocale }) {
  return getLocalePaths("artist", locales, defaultLocale, true);
}

export async function getStaticProps({ locale, params, preview = false }) {
  const lang = locale.toLowerCase().replace("-", "_");
  let query = groq`*[_type=="artist" && slug.en_us.current == $slug][0]{
    ..., 
    "products" : *[_type=="product" && references(^._id)]{..., artists[]->{
            name
          },}
  }`;

  if (lang == "it_it") {
    query = groq`*[_type=="artist" && slug.it_it.current == $slug][0]{
      ..., 
      "products" : *[_type=="product" && references(^._id)]{..., artists[]->{
            name
          },}
    }`;
  }

  //const countryCode = params?.countryCode || process.env.BUILD_COUNTRY?.toLowerCase()
  const { slug = "" } = params;
  // Fetch necessary data for the blog post using params.slug
  const artist = await client.fetch(query, { slug: slug, lang: lang });

  return {
    props: {
      preview,
      countryCode: locale,
      data: {
        artist: artist,
      },
    },
  };
}
