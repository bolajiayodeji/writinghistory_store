// First, we must import the schema creator
import createSchema from "part:@sanity/base/schema-creator";

// Then import schema types from any plugins that might expose them
import schemaTypes from "all:part:@sanity/base/schema-type";

//
// documents
//
//import category from "./documents/category.js";
import home from "./documents/home.js";
import page from "./documents/page.js";
import artist from "./documents/artist.js";
import product from "./documents/product";
import variant from "./documents/variant";
import size from "./documents/size";
import taxon from "./documents/taxon";
//import submissionForm from "./documents/submissionForm.js";

//
// singletons
//
//import config from "./documents/config.js";

//
// objects
//
//import blockContent from "./objects/blockContent.js";
import blockContent from "./blockContent";
import seo from "./objects/seo.js";
import localeString from "./locale/String";
import localeText from "./locale/Text";
import localeSlug from "./locale/Slug";
import localeBlockContent from "./locale/BlockContent";

//
// blocks
//
import megaTitleBlock from "./blocks/megaTitle";
import productlistBlock from "./blocks/productList";
import artistsliderBlock from "./blocks/artistsSlider";
import artistslistBlock from "./blocks/artistsList";

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: "default",
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    // The following are document types which will appear
    // in the studio.
    home,
    product,
    artist,
    page,
    variant,
    size,
    taxon,
    // When added to this list, object types can be used as
    // { type: "typename" } in other document schemas
    blockContent,
    localeString,
    localeText,
    localeSlug,
    localeBlockContent,
    seo,
    //blocks
    megaTitleBlock,
    productlistBlock,
    artistsliderBlock,
    artistslistBlock
  ]),
});
