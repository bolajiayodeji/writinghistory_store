import ArtistsListBlock from "/components/blocks/ArtistsListBlock";
import MegatitleBlock from "/components/blocks/MegatitleBlock";
import Layout from "../components/layout";
import { client, getLocalePaths } from "../utils/sanity/api";
import groq from "groq";
import _ from "lodash";
import { useRouter } from "next/router";
import ErrorPage from "next/error";
import HeaderPage from "../components/HeaderPage";

export default function Page({ data, preview, countryCode }) {
  const router = useRouter();
  const locale = router.locale;
  const lang = locale.toLowerCase().replace("-", "_");
  const slug = data?.post?.slug[`${lang}`].current;
  const post = data?.post;
  const titleCapitalize = slug.charAt(0).toUpperCase() + slug.slice(1);

  if (!router.isFallback && !slug) {
    return <ErrorPage statusCode={404} />;
  }

  if (!post?.blocks) {
    return <ErrorPage statusCode={404} />;
  }

  const blocks = post.blocks.map((el) => {
    if (el._type == "megatitleBlock") {
      return <MegatitleBlock block={el} key={el._key} />;
    }
    if (el._type == "productlistBlock") {
      return <ProductsList block={el} key={el._key} />;
    }
    if (el._type == "artistsliderBlock") {
      return <SliderArtists block={el} key={el._key} />;
    }
    if (el._type == "artistslistBlock") {
      return <ArtistsListBlock block={el} key={el._key} />;
    }
  });

  return (
    <>
      <Layout post={post}>
        <HeaderPage title={titleCapitalize} />
        {blocks}
      </Layout>
    </>
  );
}

export async function getStaticPaths({ locales, defaultLocale }) {
  return getLocalePaths("page", locales, defaultLocale, true);
}

export async function getStaticProps({ locale, params, preview = false }) {
  const lang = locale.toLowerCase().replace("-", "_");
  let query = groq`*[_type=="page" && slug.en_us.current == $slug][0]{...,blocks[]{..., _type== "artistslistBlock" => { ..., "artists": *[_type=="artist"] | order(name[$lang] asc) }}}`;

  console.log(lang);
  if (lang == "it_it") {
    query = groq`*[_type=="page" && slug.it_it.current == $slug][0]{...,blocks[]{..., _type== "artistslistBlock" => { ..., "artists": *[_type=="artist"] | order(name[$lang] asc) }}}`;
  }

  //const countryCode = params?.countryCode || process.env.BUILD_COUNTRY?.toLowerCase()
  const { slug = "" } = params;
  // Fetch necessary data for the blog post using params.slug
  const post = await client.fetch(query, { slug: slug, lang: lang });

  console.log("POST", post);
  return {
    props: {
      preview,
      countryCode: locale,
      data: {
        post: post,
      },
    },
  };
}
