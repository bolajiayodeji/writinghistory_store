const path = require("path");

module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
    prependData: `@import "libs/_variables.scss";`,
  },
  images: {
    domains: ["cdn.sanity.io"],
    loader: "custom",
  },
  trailingSlash: true,
  i18n: {
    // These are all the locales you want to support in
    // your application
    locales: ["en-US", "it-IT"],
    // This is the default locale you want to be used when visiting
    // a non-locale prefixed path e.g. `/hello`
    defaultLocale: "en-US",
    localeDetection: false,
  },
};
